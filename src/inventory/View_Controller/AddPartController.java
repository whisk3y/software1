package inventory.View_Controller;

import inventory.model.Inhouse;
import inventory.model.Outsourced;
import inventory.model.Part;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;


public class AddPartController {
    /**
     * Initializes the controller class.
     */
    @FXML
    TextField PartTypeField;
    @FXML
    TextField partName;
    @FXML
    TextField partId;
    @FXML
    TextField partPrice;
    @FXML
    TextField partInStock;
    @FXML
    TextField partMin;
    @FXML
    TextField partMax;
    @FXML
    RadioButton outsourcedButton;
    @FXML
    RadioButton inHouseButton;
    @FXML
    Button savePart;
    @FXML
    Label partTypeLabel;
    @FXML
    Button cancelButton;

    private boolean updatePart;

    private int newPartId;
    private ObservableList<Part> newParts = FXCollections.observableArrayList();

    public int getNewPartId() {
        return newPartId;
    }

    public void setNewPartId(int newPartId) {
        this.newPartId = newPartId;
    }

    public ObservableList<Part> getNewParts() {
        return newParts;
    }

    public void setNewParts(ObservableList<Part> newParts) {
        this.newParts = newParts;
    }



//    public void initialize() {
//
//        PartTypeField.setVisible(false);
//        partTypeLabel.setVisible(false);
//        outsourcedButton.selectedProperty().setValue(false);
//        inHouseButton.selectedProperty().setValue(false);
//    }

    /**
     * This is what auto populates the fields when updating a part
     * @param part The part that is to be updated
     */
    @FXML
    void initData(Part part) {

        if (part instanceof Inhouse) {
            setPartInHouse(part);
        }
        if (part instanceof Outsourced) {
            setPartOutsourced(part);
        }

    }

    void setUpdatePart(boolean bool) {
        this.updatePart = bool;
    }

    void setPartId(int partId) {
        this.partId.setText(Integer.toString(partId));

    }
    /**
     * When modifyPartButton is clicked in the MainInventoryView this is set to auto populate the
     * fields in the view as inHousePart. It also disables theOutsourced button
     * @param part
     */
    @FXML
    private void setPartInHouse(Part part) {
        Inhouse importedPart = (Inhouse) part;

        inHouseButton.setSelected(true);
        outsourcedButton.setDisable(true);
        partTypeLabel.setVisible(true);
        partTypeLabel.textProperty().setValue("Machine ID");
        PartTypeField.setVisible(true);
        PartTypeField.setText(Integer.toString(importedPart.getMachineID()));
        partId.setText(Integer.toString(importedPart.getPartId()));
        partId.setDisable(true);
        partName.setText(importedPart.getName());
        partInStock.setText(Integer.toString(importedPart.getInStock()));
        partPrice.setText(Double.toString(importedPart.getPrice()));
        partMin.setText(Integer.toString(importedPart.getMin()));
        partMax.setText(String.valueOf(importedPart.getMax()));
    }

    /**
     * When modifyPartButton is clicked in the MainInventoryView this is set to auto populate the
     * fields in the view as outsorcedPart. It also disables theIn-House button
     * @param part
     */
    @FXML
    private void setPartOutsourced(Part part) {
        Outsourced importedPart = (Outsourced) part;

        outsourcedButton.setSelected(true);
        inHouseButton.setDisable(false);
        //Set labels and fields visible and disabled
        partTypeLabel.setVisible(true);
        partTypeLabel.textProperty().setValue("Company Name");
        PartTypeField.setVisible(true);
        PartTypeField.setText(importedPart.getCompanyName());
        partId.setText(Integer.toString(importedPart.getPartId()));
        partId.setDisable(true);
        partName.setText(importedPart.getName());
        partInStock.setText(Integer.toString(importedPart.getInStock()));
        partPrice.setText(Double.toString(importedPart.getPrice()));
        partMin.setText(Integer.toString(importedPart.getMin()));
        partMax.setText(String.valueOf(importedPart.getMax()));

    }


    private int getPartIndex(ObservableList<Part> partsList, Part part) {
        // find the index of a part so that we can update it in an ArrayList
        for (Part searchPart: partsList) {
            if (searchPart.getPartId() == part.getPartId()) {
               return partsList.indexOf(searchPart);
            }

        }

        return -1;
    }


    /**
     * Inventory must be between the minimum or maximum value for that Part or Product
     * Maximum must have a value greater than minimum
     * Minimum must have a value less than maximum
     * @param min Minimum inventory required
     * @param max Maximum inventory allowed
     * @param inStock Current stock of part
     * @return Returns boolean for validation
     */
    boolean validateInventoryData(int min, int max, int inStock) {
        //Inventory must be between the minimum or maximum value for that Part or Product
        //Maximum must have a value greater than minimum
        //Minimum must have a value less than maximum

        if (min <= inStock) if (inStock <= max) if (max > min) if (min < max) return true;
        executeInventoryAlert();
        return false;
    }


    /**
     * Creates a new Inhouse or Outsourced part based on the Radio button selected
     * @return Returns the created Part
     * @throws IOException
     */
    @FXML
    private Part addPart() throws IOException{
        //Create an outsuorced part if the radio is selected
        if (outsourcedButton.isSelected()) {
            return new Outsourced(Integer.parseInt(partId.getText()), partName.getText(),
                    Double.parseDouble(partPrice.getText()),
                    Integer.parseInt(partInStock.getText()),
                    Integer.parseInt(partMin.getText()),
                    Integer.parseInt(partMax.getText()),
                    PartTypeField.getText());
        }
        //create an inhouse part if inhouse selected
        if (inHouseButton.isSelected()) {
            return new Inhouse(Integer.parseInt(partId.getText()),
                    partName.getText(),
                    Double.parseDouble(partPrice.getText()),
                    Integer.parseInt(partInStock.getText()),
                    Integer.parseInt(partMin.getText()),
                    Integer.parseInt(partMax.getText()),
                    Integer.parseInt(PartTypeField.getText()));
        }

        return null;
    }

    /**
     * Change the partTypeLabel and  the partTypeField for Outsourced part type
     * @throws IOException
     */
    @FXML
    private void setPartOutsourced() throws IOException {
        partTypeLabel.textProperty().setValue("Company Name");
        partTypeLabel.setVisible(true);
        PartTypeField.setVisible(true);
    }


    /**
     * Change the partTypeLabel and the partTypeField for Inhouse part type
     * @throws IOException
     */
    @FXML
    // This is used to set properties of an Inhouse part to be updated. Called from the InventoryMain
    private void setPartInHouse() throws IOException {
        partTypeLabel.textProperty().setValue("Machine ID");
        partTypeLabel.setVisible(true);
        PartTypeField.setVisible(true);
    }


    // a re-usable boolean confirmation prompt for the save/cancel buttons
    boolean booleanPrompt(String title, String message) {
        Alert partsPrompt = new Alert(Alert.AlertType.CONFIRMATION);
        partsPrompt.setTitle(title);
        partsPrompt.setContentText(message);

        ButtonType buttonAdd = new ButtonType("Add/Stay");
        ButtonType buttonReturn  = new ButtonType("Return");


        partsPrompt.getButtonTypes().setAll(buttonAdd, buttonReturn);

        Optional<ButtonType> result = partsPrompt.showAndWait();
        return result.get() == buttonAdd;
    }


    //Used to validate for input
    private boolean isInt(String integer){
        try {
            Integer.parseInt(integer);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    //A small method to show an invalid input dialog box
    private void fieldEmptyAlert(String message) {
        Alert emptyAlert = new Alert(Alert.AlertType.INFORMATION);
        emptyAlert.setTitle("InvalidInput");
        emptyAlert.setContentText(message);
        emptyAlert.showAndWait();
    }

    // Validates all field input
    private boolean validateInputs() {

        // Make sure none of the fields are empty
        if (partName.getText().isEmpty()) {
              fieldEmptyAlert("Name field must not be blank");
              return false;
        }

        //isInt validates that the input is an integer
        if (partInStock.getText().isEmpty() || !isInt(partInStock.getText())) {
            fieldEmptyAlert("Inventory field must not be blank or is not an integer");
            return false;
        }

        if (partMin.getText().isEmpty() || !isInt(partMin.getText())) {
            fieldEmptyAlert("Min field must not be blank ");
            return false;
        }

        if (partMax.getText().isEmpty() || !isInt(partMax.getText())) {
            fieldEmptyAlert("Max field must not be blank or is not an integer");
            return false;
        }

        if (PartTypeField.getText().isEmpty()) {
            fieldEmptyAlert("MachineId/Company Name field must not be blank");
            return false;
        }

        if (partPrice.getText().isEmpty()) {
            fieldEmptyAlert("Part Price field must not be blank");

            try {
                // try to parse a double, if it fails return false and show user
                Double.parseDouble(partPrice.getText());
            } catch (NumberFormatException e) {
                fieldEmptyAlert("Part Price must be a double ex. 10.00");
                return false;
            }

            return false;
        }

        // make sure the PartTypeField is an integer, if not return false and tell user
        if (inHouseButton.isSelected()) {
            try {
                Integer.parseInt(PartTypeField.getText());
            } catch (NumberFormatException e) {
                Alert machineIdAlert = new Alert(Alert.AlertType.INFORMATION);
                machineIdAlert.setTitle("Invalid MachineID");
                machineIdAlert.setContentText("Please set an integer value for machineID.");
                machineIdAlert.showAndWait();
                return false;
            }
        }

        //all if statements passed, return true to continue saving part
        return true;
    }



    /**
     * When the save button is clicked this validates the inventory values and if true
     * calls the addPart method to create the part. It then prepares to load the MainInventoryView and updates the
     * PartsTable with the newly created Part or modified part. If validation of the min, max, and inStock fails, return to main.
     * @param event Event information
     * @throws IOException
     */
    @FXML
    public void saveAndReturnToMain(ActionEvent event) throws IOException {

        //Load Inventory Main
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("InventoryMain.fxml"));
        Parent root = loader.load();

        Scene mainScene = new Scene(root);

        //Grab the partslist table to either update or add a new part
        InventoryController inventoryController = loader.getController();
        ObservableList<Part> partsList = inventoryController.PartsTable.getItems();

        //First make sure all fields are filled out
        if (validateInputs()) {
            //Set inventory related values for validation
            int min = Integer.parseInt(partMin.getText());
            int max = Integer.parseInt(partMax.getText());
            int inv = Integer.parseInt(partInStock.getText());

            //Make sure the inventory falls within the min and max range
            if(validateInventoryData(min, max, inv)) {

            //Check if a part has been passed in to be updated.
            //If so update the part and return to main window.
            if (updatePart) {

                //Create the updated part
                Part newPart = addPart();
                int index = getPartIndex(partsList, newPart);

                // If no index is found tell the user and do nothing else
                if (index == -1) {
                    Alert alertMessage = new Alert(Alert.AlertType.ERROR);
                    alertMessage.setTitle("Index Error");
                    alertMessage.setContentText("Unable to get the index of the Part.");
                    alertMessage.showAndWait();
                    return;
                }
                //See if the updated part is in the list passed from InventoryMainView
                newParts.set(index, newPart);

            } else {
                //if updatePart is false create a new part and add it to newParts list
                Part newPart = addPart();
                boolean added = newParts.add(newPart);

                //if the part was added to the list notify user it was successful
                if (added) {
                    inventoryController.displayAlert("Added", "Part Added", "Your part has been added.");
                }

                //Ask the user if they want to add more parts or return to main
                boolean addMore = booleanPrompt("Add Parts", "Add more parts or return to main Menu");

                //If add is selected then the form is cleared and a new partId is generated
                if (addMore) {
                    //Generate a new partId and clear all fields for the next part to be added
                    int newId = Integer.parseInt(partId.getText()) + 1;
                    partId.clear();
                    partId.setText(Integer.toString(newId));
                    partId.setDisable(true);
                    partName.clear();
                    partInStock.clear();
                    partPrice.clear();
                    partMin.clear();
                    partMax.clear();
                    inHouseButton.setSelected(false);
                    outsourcedButton.setSelected(false);
                    PartTypeField.clear();
                    return;
                }

            }

            }
        } else {
            //if validateInputs returns false do nothing else
            return;
        }

        // once everything has been validated and parts created clear the passed partslist
        // and add our new parts.
        partsList.clear();
        partsList.addAll(newParts);
        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        //change scene
        window.setScene(mainScene);
        window.show();


    }

    @FXML
    private void executeInventoryAlert() {
        // this alert is called when validating the correct inventory values
        Alert inventoryAlert = new Alert(Alert.AlertType.INFORMATION);
        inventoryAlert.setHeaderText("Inventory Alert");
        inventoryAlert.setTitle("Mismatched Values");
        inventoryAlert.setContentText("Either the inventory/in stock level is not within the Min and Max range" +
                " or the min and max values are not less than or greater than one another");
        inventoryAlert.showAndWait();
    }

    /**
     * Return to main screen
     * @throws IOException
     */
    @FXML
    public void returnToMain(ActionEvent event) throws IOException{
        // Ask if the user wants to cancel
        boolean stay = booleanPrompt("Return to main", "Are you sure you want to cancel and return to main menu? All actions will be lost");

        if (!stay) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("InventoryMain.fxml"));
            Parent root = loader.load();

            Scene mainScene = new Scene(root);

            //This line gets the Stage information
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

            window.setScene(mainScene);
            window.show();
        }
    }

}