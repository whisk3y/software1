package inventory.View_Controller;


import inventory.MainApp;
import inventory.model.Inhouse;
import inventory.model.Outsourced;
import inventory.model.Part;
import inventory.model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;


public class InventoryController {
    @FXML
    public ObservableList<Part> Parts = FXCollections.observableArrayList();
    @FXML
    public ObservableList<Product> Products = FXCollections.observableArrayList();

    //Define Parts Table
    @FXML
    public TableView<Part> PartsTable;
    // Define Products Table
    @FXML
    public TableView<Product> ProductsTable;
    @FXML
    Button modifyPartButton;
    @FXML Button addProductButton;
    @FXML Button deletePartButton;
    @FXML Button exitButton;
    @FXML AddPartController addPartController;
    @FXML
    private TableColumn<Part, Integer> PartID;
    @FXML
    private TableColumn<Part, String> PartName;
    @FXML
    private TableColumn<Part, Integer> PartInventoryLevel;
    @FXML
    private TableColumn<Part, Double> PartPrice;
    @FXML
    private TableColumn<Product, Integer> ProductID;
    @FXML
    private TableColumn<Product, String> ProductName;
    @FXML
    private TableColumn<Product, Integer> ProductInventoryLevel;
    @FXML
    private TableColumn<Product, Double> ProductPrice;
    //Define Search Text Fields
    @FXML
    private TextField SearchPartField;
    @FXML
    private TextField SearchProductField;
    @FXML
    private Button modifyProductButton;
    @FXML
    private Button deleteProductButton;
    @FXML
    private Button searchPartButton;
    @FXML
    private Button searchProductButton;
    @FXML
    Button addPartButton;


    private MainApp MainApp;

    public TableView<Part> getPartsTable() {
        return PartsTable;
    }

    public void setPartsTable(TableView<Part> partsTable) {
        PartsTable = partsTable;
    }

    public ObservableList<Part> getParts() {
        return PartsTable.getItems();
    }

    public void setParts(ObservableList<Part> parts) {
        Parts = parts;
    }

    public ObservableList<Product> getProducts() {
        return Products;
    }

    public void setProducts(ObservableList<Product> products) {
        Products = products;
    }

    public inventory.MainApp getMainApp() {
        return MainApp;
    }



    // Set Parts List sample Data
    private ObservableList<Part> initParts() {
        // int partId, String name, double price, int inStock, int min, int max, String companyName - Outsourced
        // int partId, String name, double price, int inStock, int min, int max, int machineID - InHouse
        ObservableList<Part> parts = FXCollections.observableArrayList();
        parts.add(new Inhouse(1, "IHPart1", 2.00, 3, 1, 5, 103));
        parts.add(new Inhouse(2, "IHPart2", 5.00, 3, 2, 5, 103));
        parts.add(new Outsourced(3, "OSPart1", 6.00, 3, 2, 10, "TestLabs"));
        parts.add(new Outsourced(4, "OSPart2", 3.00, 3, 2, 15, "TesLabs"));

        return parts;
    }

    private ObservableList<Product> initProducts() {
        ObservableList<Product> products = FXCollections.observableArrayList();
        ObservableList<Part> partsList = initParts();

        // Initialize our products parts ArrayLists
        ArrayList<Part> prod1Parts = new ArrayList<>();
        ArrayList<Part> prod2Parts = new ArrayList<>();

        //Add parts to the arrayLists
        prod1Parts.add(partsList.get(0));
        prod2Parts.add(partsList.get(3));

        //Make new products
        Product prod1 = new Product(1, "Product1",
                4.00, 2, 1,13, null);
        Product prod2 = new Product(2, "Product2", 6.00, 1, 1, 15, null);

        // Set the initial parts for the products
        // Setting them in the new Product definition was not functioning properly
        prod2.setParts(prod2Parts);
        prod1.setParts(prod1Parts);

        products.add(prod1);
        products.add(prod2);
        return products ;
    }

    @FXML
    public void initialize() {

        // Init fields
        PartID.setCellValueFactory(new PropertyValueFactory<>("PartId"));
        PartName.setCellValueFactory(new PropertyValueFactory<>("Name"));
        PartInventoryLevel.setCellValueFactory(new PropertyValueFactory<>("InStock"));
        PartPrice.setCellValueFactory(new PropertyValueFactory<>("Price"));

        ProductID.setCellValueFactory(new PropertyValueFactory<>("ProductId"));
        ProductName.setCellValueFactory(new PropertyValueFactory<>("Name"));
        ProductInventoryLevel.setCellValueFactory(new PropertyValueFactory<>("InStock"));
        ProductPrice.setCellValueFactory(new PropertyValueFactory<>("Price"));

        ObservableList<Part> Parts = initParts();
        ObservableList<Product> Products = initProducts();

        PartsTable.setItems(Parts);
        PartsTable.setEditable(true);
        ProductsTable.setItems(Products);
        ProductsTable.setEditable(true);

    }

    //make a new part ID to pass into Addpart
    private int genNewPartId() {
        int newPartId;

        int tableSize = PartsTable.getItems().size();
        newPartId = PartsTable.getItems().get(tableSize - 1).getPartId() + 1;

        return newPartId;
    }


    /**
     * Navigates to AddPartView
     * @param event event data
     * @throws IOException
     */
    @FXML
    private void addPartView(ActionEvent event) throws IOException{

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("AddPart.fxml"));
        Parent root = loader.load();
        Scene addPartScene = new Scene(root);
        AddPartController addPartController = loader.getController();

        int newPartId = genNewPartId();
        //Pass in the new partId and disable the partId field
        addPartController.setPartId(newPartId);
        addPartController.partId.setDisable(true);
        addPartController.setNewParts(PartsTable.getItems());

        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

        window.setScene(addPartScene);
        window.show();
    }

    /**
     * Opens the Modify Part view and passes the selected part to be modified
     * @param event event data
     * @throws IOException
     */
    @FXML
    private void modifyPartView(ActionEvent event) throws IOException{

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("AddPart.fxml"));


        Parent root = loader.load();

        Scene modifyPartScene = new Scene(root);
        AddPartController addPartController = loader.getController();

        //get the selected item in the table to pass to add part view
        Part selectedItem = PartsTable.getSelectionModel().getSelectedItem();
        //If no part is selected show a dialog box
        if (selectedItem == null) {
            displayAlert("Modify Part", "Modify Part", "No part selected to modify.");
            return;
        }

        addPartController.initData(selectedItem);
        //Pass the part we want to update
        addPartController.setNewParts(PartsTable.getItems());
        // Specify we are updating a part
        addPartController.setUpdatePart(true);

        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

        window.setScene(modifyPartScene);
        window.show();
    }


    /**
     * Find and select an item in a tableview
     * @param event event data
     * @throws IOException throw ioexception
     */
    public void lookupPart(ActionEvent event) throws IOException {

        String partSearchItem = SearchPartField.getText();
        boolean found = false;
        int itemNumber;

        //Make sure we can parse an int from the SearchPartField if we can't set -1
        try {
            itemNumber = Integer.parseInt(partSearchItem);
        } catch (NumberFormatException e) {
            itemNumber = -1;
        }

        if (event.getSource() == searchPartButton) {
            ObservableList<Part> parts = PartsTable.getItems();
            //Search the PartsTable to see if the part is in the table. Select and scrollTo it if it is
            for (Part p : parts) {
                if (p.getPartId() == itemNumber || p.getName().equals(partSearchItem)) {
                    PartsTable.getSelectionModel().select(p);
                    PartsTable.scrollTo(p);
                    found = true;
                }
            }
        }
        //Handle search the products table
        if (event.getSource() == searchProductButton) {
            String productSearchItem = SearchProductField.getText();

            //Parse an integer to search with product id. Catch the exception and set itemnumber to -1 if it fails
            try{
                itemNumber = Integer.parseInt(productSearchItem);
            } catch (NumberFormatException e) {
                itemNumber = -1;
            }

            ObservableList<Product> productsList = ProductsTable.getItems();
            //Search the ProductsTable to see if the product is in the table. Select and scrollTo it if it is
            for (Product product: productsList) {
                if (product.getProductId() == itemNumber || product.getName().equals(productSearchItem)) {
                    ProductsTable.getSelectionModel().select(product);
                    ProductsTable.scrollTo(product);
                    found = true;
                }
            }
        }
        if (!found) {
            displayAlert("Not Found", "Not Found", "Part or Product was not found.");
        }

    }


    /**
     * Used to generate the next productId in Add Product view
     * @return last product id
     */
    private int getLastProductId() {
        ObservableList<Product> products = ProductsTable.getItems();

        int productsLen = products.size();
        Product prod =  products.get(productsLen - 1);
        return prod.getProductId();
    }

    //Used in the handleDeleteButtons method to remove a part from a product
    // Aims to prevent parts associated with products from being deleted
    private void removePartFromProduct(Part part) {
        ProductsTable.getItems().forEach((Product) -> {
            if (Product.getAssociatedParts().size() >= 2) {
                if (Product.removeAssociatedPart(part.getPartId())) {
                    System.out.print("Removed " + part.getName() + " from " + Product);
                }
            }
            else {
                displayAlert("Delete", "Delete Error", "A product must have more than 1 Part.");
                return;
            }
        });
    }

    /**
     * Handles the delete buttons for each tableview. If a part is deleted then it is searched for in all the products
     * and deleted from all associated products
     * @param event event data
     */
    @FXML
    private void handleDeleteButtons(ActionEvent event) {
        boolean removed;

        //Get the item to be deleted and grab all the parts in the table
        Part selectedPart = PartsTable.getSelectionModel().getSelectedItem();
        ObservableList<Part> allParts = PartsTable.getItems();

        //Get the item to be deleted and grab all the products in the table
        Product selectedProduct = ProductsTable.getSelectionModel().getSelectedItem();
        ObservableList<Product> allProducts = ProductsTable.getItems();

       


        if (event.getSource() == deletePartButton) {
             if (selectedPart == null ) {
                displayAlert("Delete", "Delete", "No part selected to delete.");
                return;
            }
            //Remove the selected part from all products that it is associated with
            removed = deleteConfirm() && allParts.remove(selectedPart);
            //Remove associated parts from products if the product has more than 1 part

            removePartFromProduct(selectedPart);
            if (!removed) {
                displayAlert("Delete", "Delete Failed", "Product not Deleted.");
            }
        }

        if (event.getSource() == deleteProductButton) {
            
             if (selectedProduct == null ) {
                displayAlert("Delete", "Delete", "No product selected to delete.");
                return;
            }
            //Confirm delete and remove part
            removed = deleteConfirm() && allProducts.removeAll(selectedProduct);
            if (!removed) {
                displayAlert("Delete", "Delete Failed", "Product not Deleted.");
            }
        }


    }


    /**
     * Handles the Add/Modify product buttons. If Modify is hit the method passes the selected Product to the
     * AddProductController and prefills the product fields as well as populates the productPartsTableView.
     * @param event event data
     * @throws IOException throws exception
     */
    @FXML
    private void handleProductButtons(ActionEvent event) throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("AddProduct.fxml"));
        //access the controller and call a method

        Parent root = loader.load();

        Scene modifyPartScene = new Scene(root);


        //Populate our AvailableParts Table
        AddProductController addProductController = loader.getController();
        addProductController.setAvailableParts(PartsTable.getItems());
        addProductController.partsTableView.setItems(PartsTable.getItems());
        addProductController.initAvailablePartsTable();


        if (event.getSource() == modifyProductButton) {
            Product selectedProduct = ProductsTable.getSelectionModel().getSelectedItem();

            if (selectedProduct == null) {
                displayAlert("Modify Product", "Modify Product", "No product selected to modify.");
                return;
            }
            //Pass the product to be modified
            addProductController.setUpdate(true);
            addProductController.initFieldData(selectedProduct);
            addProductController.setCurrentProduct(selectedProduct);
            ObservableList<Part> productParts = FXCollections.observableArrayList(selectedProduct.getAssociatedParts());
            addProductController.productPartsTableView.setItems(productParts);
            addProductController.setNewProducts(ProductsTable.getItems());

        } else {
            //Get the last productID and pass it to Add/Modify Product view
            int lastId = getLastProductId();
            addProductController.setNewProducts(ProductsTable.getItems());
            addProductController.setProductId(lastId + 1);
        }

        //This line gets the Stage information
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();

        window.setScene(modifyPartScene);
        window.show();

    }

    /**
     * Confirm the deletion of an object
     * @return true or false
     */
    boolean deleteConfirm() {
        Alert deletePrompt = new Alert(Alert.AlertType.CONFIRMATION);
        deletePrompt.setTitle("Delete Confirmation");
        deletePrompt.setHeaderText("Confirm Deleteing of items(s)");
        deletePrompt.setContentText("Are you sure you want to delete these items?");

        ButtonType buttonYes = new ButtonType("Yes");
        ButtonType buttonNo  = new ButtonType("No");


        deletePrompt.getButtonTypes().setAll(buttonYes, buttonNo);

        Optional<ButtonType> result = deletePrompt.showAndWait();
        return result.get() == buttonYes;
    }

    /**
     * Helper method. Re-usable alert dialog box
     * @param title
     * @param header
     * @param message
     */
    void displayAlert(String title, String header, String message) {
        Alert alertMessage = new Alert(Alert.AlertType.INFORMATION);
        alertMessage.setTitle(title);
        alertMessage.setHeaderText(header);
        alertMessage.setContentText(message);
        alertMessage.showAndWait();
    }

    public void exitApp() {
        System.exit(0);
    }

}
