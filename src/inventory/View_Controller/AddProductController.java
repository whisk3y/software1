package inventory.View_Controller;

import inventory.model.Part;
import inventory.model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;


public class AddProductController {

    @FXML
    public TableView<Part> partsTableView;
    @FXML
    Label productHeaderLabel;
    private ObservableList<Part> availableParts;
    private ObservableList<Part> ProductParts;
    private AddPartController addPartController = new AddPartController();
    private InventoryController inventoryControllerNew = new InventoryController();
    @FXML
    private TextField productId;
    @FXML
    private TextField productName;

    @FXML
    private TextField productinStock;

    @FXML
    private TextField productPrice;

    @FXML
    private TextField productMin;

    @FXML
    private TextField productMax;
    @FXML
    private TableColumn<Part, Integer> partIdColumn;
    @FXML
    private TableColumn<Part, String> partNameColumn;
    @FXML
    private TableColumn<Part, Integer> partInStockColumn;
    @FXML
    private TableColumn<Part, Double> partPriceColumn;
    @FXML
    // Used but separated button handles into functions
    Button partSearchButton;
    @FXML
    private TextField partSearchField;
    @FXML
    // Used but separated button handles into functions
    Button addPartButton;
    @FXML
    TableView<Part> productPartsTableView;
    @FXML
    private TableColumn<Part, Integer> productPartIdColumn;
    @FXML private TableColumn<Part, String> productPartNameColumn;
    @FXML private TableColumn<Part, Integer> productPartInStockColumn;
    @FXML
    private TableColumn<Part, Double> productPartPriceColumn;
    @FXML
    Button deleteProductPartButton;
    @FXML
    Button productSaveButton;
    @FXML
    Button cancelButton;

    private Product currentProduct;

    ObservableList<Product> newProducts = FXCollections.observableArrayList();

    public void setNewProducts(ObservableList<Product> newProducts) {
        this.newProducts = newProducts;
    }
    private boolean isUpdate;

    void setProductId(int id) {
        productId.setText(Integer.toString(id));
        productId.setDisable(true);
    }


    void setUpdate(boolean update) {
        isUpdate = update;
    }

    private Product getCurrentProduct() {
        return currentProduct;
    }

    void setCurrentProduct(Product currentProduct) {
        this.currentProduct = currentProduct;
    }

    public void setAddPartController(AddPartController addPartController) {
        this.addPartController = addPartController;
    }

    public ObservableList<Part> getAvailableParts() {
        return availableParts;
    }

    void setAvailableParts(ObservableList<Part> availableParts) {
        this.availableParts = availableParts;
    }

    private ObservableList<Part> getProductParts() {
        return ProductParts;
    }

    private void setProductParts(ObservableList<Part> productParts) {
        ProductParts = productParts;
    }


    /**
     * This is called from the referrer window to pre-populate the partsTableView
     */
    @FXML
    void initAvailablePartsTable() {
        // Init fields
        partIdColumn.setCellValueFactory(new PropertyValueFactory<>("PartId"));
        partNameColumn.setCellValueFactory(new PropertyValueFactory<>("Name"));
        partInStockColumn.setCellValueFactory(new PropertyValueFactory<>("InStock"));
        partPriceColumn.setCellValueFactory(new PropertyValueFactory<>("Price"));

    }

    @FXML
    void initialize() {
        //Initialize the productPartsTable
        productPartsTableView.setEditable(true);

        productPartIdColumn.setCellValueFactory(new PropertyValueFactory<>("PartId"));
        productPartNameColumn.setCellValueFactory(new PropertyValueFactory<>("Name"));
        productPartInStockColumn.setCellValueFactory(new PropertyValueFactory<>("InStock"));
        productPartPriceColumn.setCellValueFactory(new PropertyValueFactory<>("Price"));


    }

    /**
     * If we are updating a product this method is used to pre-fill the fields and lock the product ID field so
     * it cannot be changed
     * @param product
     */
    @FXML
    void initFieldData(Product product) {
        productId.setText(Integer.toString(product.getProductId()));
        productId.setDisable(true);

        productName.setText(product.getName());
        productinStock.setText(Integer.toString(product.getInStock()));
        productPrice.setText(Double.toString(product.getPrice()));
        productMin.setText(Integer.toString(product.getMin()));
        productMax.setText(Integer.toString(product.getMax()));

        productHeaderLabel.setText("Update Part");

    }

    private boolean existsInTable(Part item) {
        for (Part part: productPartsTableView.getItems()){
            if (part.getPartId() == item.getPartId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Adds a selected part to the productPartsTableView after it verifies it does not already exist
     */
    @FXML
    void handelAddPartButton() {

        Part selectedPart = partsTableView.getSelectionModel().getSelectedItem();
        ObservableList<Part> currentList = productPartsTableView.getItems();

        if (currentList.size() == 0) {
            currentList.add(selectedPart);
            setProductParts(currentList);
            productPartsTableView.setItems(ProductParts);
        }

        else{
            boolean exists = existsInTable(selectedPart);
            if (exists) {
                    alertMessage("Exists", "This part already exists for the product");
                }
                else {
                    currentList.add(selectedPart);
                }
            }
        partsTableView.getSelectionModel().clearSelection();
    }

    /**
     * Returns to main window
     * @param event
     * @throws IOException
     */
    @FXML
    void handleCancelButton(ActionEvent event) throws IOException {
        boolean stay = addPartController.booleanPrompt("Return to main", "Are you sure you want to cancel and return to main menu?");

        if (!stay) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("InventoryMain.fxml"));
            Parent root = loader.load();
            InventoryController inventoryController = loader.getController();
            inventoryController.ProductsTable.getItems().clear();
            inventoryController.ProductsTable.getItems().addAll(newProducts);
            Scene mainScene = new Scene(root);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

            window.setScene(mainScene);
            window.show();
        }
    }

    /**
     * Deletes a part from the productPartsTableView
     */
    @FXML
    void handleDeletePartButton() {

        boolean delete = inventoryControllerNew.deleteConfirm();
        if (delete) {
            ObservableList<Part> productPartsList = productPartsTableView.getItems();
            ObservableList<Part> selectedPart = productPartsTableView.getSelectionModel().getSelectedItems();
            if (productPartsList.removeAll(selectedPart)) {
                alertMessage("Delete", "Deleted Part.");
            } else {
                alertMessage("Delete", "Failed to delete part.");
            }
            productPartsTableView.getSelectionModel().clearSelection();
        }
    }

    /**
     * Searches for a part in the partsTableView and selects the part if found
     */
    @FXML
    void handlePartSearch() {
        ObservableList<Part> partsList = partsTableView.getItems();
        boolean found = false;
        String searchText = partSearchField.getText();
        try {
            int partIdParsed = Integer.parseInt(searchText);

            for (Part part: partsList) {
                if (part.getPartId() == partIdParsed) {
                    partsList.stream().filter(item -> item.getPartId() == partIdParsed).findAny().ifPresent(item -> {
                        partsTableView.getSelectionModel().select(item);
                        partsTableView.scrollTo(item);
                    });
                    found = true;
                } else {
                    found = false;
                }

            }

        }
        catch (NumberFormatException e) {
            for (Part part: partsList) {
                if (part.getName().equals(searchText)) {
                    partsList.stream().filter(item -> item.getName().equals(searchText)).findAny().ifPresent(item -> {
                        partsTableView.getSelectionModel().select(item);
                        partsTableView.scrollTo(item);
                    });
                    found = true;
                } else {
                    found = false;
                }
            }

        }

        if (!found) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText("Error");
            alert.setContentText("Part not found");

            alert.showAndWait();
        }

    }

    /**
     * Creates the new product. This does not care if theproduct is new or not
     * @return Product from the fields
     */
    private Product createNewProduct() {
        ArrayList<Part> productParts = new ArrayList<>();
        ObservableList<Part> parts = productPartsTableView.getItems();
        
        for (int i = 0; i < parts.size(); i++) {
            productParts.add(i, parts.get(i));
        }

        return new Product(Integer.parseInt(productId.getText()),
                productName.getText(),
                Double.parseDouble(productPrice.getText()),
                Integer.parseInt(productinStock.getText()),
                Integer.parseInt(productMin.getText()),
                Integer.parseInt(productMin.getText()),
                productParts);
    }


    /**
     * Make sure the cost of the Product is >= the cost of all parts
     * @return
     */
    private boolean validateCost() {
//        Ensure that the price of product cannot be less than the cost of the parts
        Double productCost = Double.parseDouble(productPrice.getText());

        //Loop through the productPartsTableView and add the cost of all parts
        Double totalPartCost = 0.0;
        boolean costValid;
        for (Part part : productPartsTableView.getItems()) {
            totalPartCost = totalPartCost + part.getPrice();
        }

        //Check if product cost is greater than or equal to total cost
        costValid = productCost >= totalPartCost;

        if (!costValid) {
            alertMessage("Invalid Cost", "Invalid cost. The price is not greater than or equal to the cost of the parts.");
            return false;
        } else{return true;}

    }

    /**
     * Validate field input
     * @return
     */
    private boolean validateProductInput() {
        //Ensure that a product must have a name, price and inventory level(default 0)
        if ((productName.getText().isEmpty()) || (productPrice.getText().isEmpty()) || (productinStock.getText().isEmpty())){
            alertMessage("Invalid Input",
                    "Please verify that none of the fields are empty.");
            return false;
        } else { return true; }

    }

    /**
     * A helper method to get product index in the ObservableArrayList so that we can modify the product in the list
     * @param productList
     * @param product
     * @return
     */
    private int getProductIndex(ObservableList<Product> productList, Product product) {
       for (Product searchProduct: productList) {
            if (searchProduct.getProductId() == product.getProductId()) {
                return productList.indexOf(searchProduct);
            }
        }
        return -1;
    }

    /**
     * A re-usable alert box
     * @param title
     * @param message
     */
    @FXML
    private void alertMessage(String title, String message) {
        Alert alertMessage = new Alert(Alert.AlertType.INFORMATION);
        alertMessage.setTitle(title);
        alertMessage.setContentText(message);
        alertMessage.showAndWait();
        }

    // If there are 1 or more parts in the table then the product is valid. If not return false
    private boolean validateProductParts() {
        ObservableList<Part> partsList = productPartsTableView.getItems();
        if (partsList.size() >= 1) {
            return true;
        } else {
            alertMessage("No Parts", "You must add parts before you can save.");
            return false;
        }
    }


    /**
     * generates a new productId for new products after the initial add product has been done
     * @param currentList the current products list
     * @return new ID
     */
    private int genNewProductId (ObservableList<Product> currentList) {
        int newId;

        int lenCurrent = currentList.size();

        newId = currentList.get(lenCurrent - 1).getProductId() + 1;

        return newId;
    }


    /**
     * Validates input fields and checks if the product is being update or is new based
     * on a passed variable from the InventoryController. Then saves or updates the product.
     * @param event
     * @throws IOException
     */
    @FXML
    void handleProductSaveButton(ActionEvent event) throws IOException {

        //set our variables used to validate inventory
        int min = Integer.parseInt(productMin.getText());
        int max = Integer.parseInt(productMax.getText());
        int inv = Integer.parseInt((productinStock.getText()));

        //Configure boolean variables to make sure input is correct
        boolean inputValid = validateProductInput();
        boolean inventoryValid = addPartController.validateInventoryData(min,max,inv);
        boolean costValid = validateCost();
        boolean added;
        boolean partsAdded = validateProductParts();
        boolean addMore;

        //pre-load data for the scene switching when done creating new products
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("InventoryMain.fxml"));
        Parent root = loader.load();

        Scene mainScene = new Scene(root);

        //get the current Products table items
        InventoryController inventoryController = loader.getController();
        ObservableList<Product> currentProductsList = inventoryController.ProductsTable.getItems();


        // Make sure all input is valid and make new Product if true
        if (inputValid && inventoryValid && costValid && partsAdded) {

            Product newProduct;
            newProduct = createNewProduct();

            if (isUpdate) {

                // Get the index of the current product
                int index = getProductIndex(newProducts, newProduct);

                // if the product to update is not found -1 is returned  and we add it to the list of products
                if (index == -1) {
                    alertMessage("Index Error",
                            "The product was not found in the list to update. Adding to Product list.");
                    newProducts.add(newProduct);

                }

                // Update the product in the list
                newProducts.set(index, newProduct);

                //Need to clear the products list
                currentProductsList.clear();
                //Then add newProducts to the currentProductsList so that no data is lost
                currentProductsList.addAll(newProducts);

                //Switch to inventorymain view
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(mainScene);
                window.show();

            } else {
                //Since isUpdate is false we are creating a new product
                added = newProducts.add(newProduct);
                // if the new product was added prompt user to create more or return to main
                if (added) {
                    addMore = addPartController.booleanPrompt("Product Added", "Would you like to add more products or return to main menu?");
                    //Clear fields if more products are going to be added
                    if (addMore) {
                        productId.clear();
                        productId.setText(Integer.toString(genNewProductId(newProducts)));
                        productId.setDisable(true);
                        productName.clear();
                        productinStock.clear();
                        productPrice.clear();
                        productMin.clear();
                        productMax.clear();
                        productPartsTableView.setEditable(true);
                        productPartsTableView.getItems().clear();
                    } else {
                        // if no more products will be created save the products and write back to inventoryMain view
                        currentProductsList.clear();
                        currentProductsList.addAll(newProducts);
                        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                        window.setScene(mainScene);
                        window.show();
                    }
                } else {
                    alertMessage("Add Failed", "The " +
                            "program failed to add your new product.");
                }

            }

        }

    }
}