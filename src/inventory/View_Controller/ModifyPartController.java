package inventory.View_Controller;

/**
 * Created by dhendel on 7/24/17.
 */
//public class ModifyPartController {
//    @FXML
//    RadioButton outsourcedButton;
//    @FXML
//    RadioButton inHouseButton;
//    @FXML
//    Label partTypeLabel;
//    @FXML
//    TextField PartTypeField;
//
//    @FXML
//    TextField partName;
//    @FXML
//    TextField partId;
//    @FXML
//    TextField partPrice;
//    @FXML
//    TextField partInStock;
//    @FXML
//    TextField partMin;
//    @FXML
//    TextField partMax;
//
//    @FXML
//    private Part part;
//
//    private AddPartController addPartController;
//
//    public Part getPart() {
//        return part;
//    }



//    @FXML
//    void initData(Part part) {
//
//        if (part instanceof Inhouse) {
//            setPartInHouse(part);
//        }
//        if (part instanceof Outsourced) {
//           setPartOutsourced(part);
//        }
//
//    }

//    @FXML
//    private void setPartOutsourced(Part part) {
//        Outsourced importedPart = (Outsourced) part;
//
//        outsourcedButton.setSelected(true);
//        //Set labels and fields visible and disabled
//        partTypeLabel.setVisible(true);
//        partTypeLabel.textProperty().setValue("Company Name");
//        PartTypeField.setVisible(true);
//        PartTypeField.setText(importedPart.getCompanyName());
//        partId.setText(Integer.toString(importedPart.getPartId()));
//        partId.setDisable(true);
//        partName.setText(importedPart.getName());
//        partInStock.setText(Integer.toString(importedPart.getInStock()));
//        partPrice.setText(Double.toString(importedPart.getPrice()));
//        partMin.setText(Integer.toString(importedPart.getMin()));
//        partMax.setText(String.valueOf(importedPart.getMax()));
//
//    }

//    private Outsourced modifyOutsourcedPart() {
//        return new Outsourced(Integer.parseInt(partId.getText()), partName.getText(),
//                Double.parseDouble(partPrice.getText()),
//                Integer.parseInt(partInStock.getText()),
//                Integer.parseInt(partMin.getText()),
//                Integer.parseInt(partMax.getText()),
//                PartTypeField.getText());
//    }

//    @FXML
//    private void setPartInHouse(Part part) {
//        Inhouse importedPart = (Inhouse) part;
//
//        inHouseButton.setSelected(true);
//        partTypeLabel.setVisible(true);
//        partTypeLabel.textProperty().setValue("Machine ID");
//        PartTypeField.setVisible(true);
//        PartTypeField.setText(Integer.toString(importedPart.getMachineID()));
//        partId.setText(Integer.toString(importedPart.getPartId()));
//        partId.setDisable(true);
//        partName.setText(importedPart.getName());
//        partInStock.setText(Integer.toString(importedPart.getInStock()));
//        partPrice.setText(Double.toString(importedPart.getPrice()));
//        partMin.setText(Integer.toString(importedPart.getMin()));
//        partMax.setText(String.valueOf(importedPart.getMax()));
//    }

//    private Inhouse modifyInhousePart() {
//        return new Inhouse(Integer.parseInt(partId.getText()), partName.getText(),
//                Double.parseDouble(partPrice.getText()),
//                Integer.parseInt(partInStock.getText()),
//                Integer.parseInt(partMin.getText()),
//                Integer.parseInt(partMax.getText()),
//                Integer.parseInt(PartTypeField.getText()));
//    }


//    @FXML
//    private void updateAndReturn(ActionEvent event) throws IOException {
//        int min = Integer.parseInt(partMin.getText());
//        int max = Integer.parseInt(partMax.getText());
//        int inv = Integer.parseInt(partInStock.getText());
//
//        setAddPartController(addPartController);
//
//        boolean dataValid = addPartController.validateInventoryData(min, max, inv);
//
//        if (dataValid){
//            Part updatedPart = addPartController.addPart();
//
//            FXMLLoader loader = new FXMLLoader();
//            loader.setLocation(getClass().getResource("InventoryMain.fxml"));
//            Parent root = loader.load();
//
//            Scene mainScene = new Scene(root);
//
//            //Update the part in the PartsTable
//            InventoryController inventoryController = loader.getController();
//            ObservableList partsList = inventoryController.PartsTable.getItems();
//            int index = partsList.indexOf(updatedPart);
//            inventoryController.PartsTable.getItems().set(index, updatedPart);
//
//            //This line gets the Stage information
//            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
//
//            window.setScene(mainScene);
//            window.show();
//        } else {
//            addPartController.executeInventoryAlert();
//        }
//
//
//    }
//
//    @FXML
//    private void cancel(ActionEvent event) throws IOException{
//        addPartController.returnToMain(event);
//    }



//}
