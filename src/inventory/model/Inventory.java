/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventory.model;

import java.util.ArrayList;

/**
 *
 * @author dustin
 */
public class Inventory {

    private ArrayList<Product> products;
    private ArrayList<Part> parts;



    public void addProduct(Product product) {
        products.add(product);
    }

    public Product lookupProduct(int productID){
        for (Product product: products) {
            if (product.getProductId() == productID) {
                return product;
            }
        }
        return null;
    }

    private int getProductIndex(int productId) {
        for (Product searchProduct:products) {
            if (searchProduct.getProductId() == productId) {
                return products.indexOf(searchProduct);
            }

        }
        return -1;
    }

    public boolean removeProduct(int productID){
        int index = getProductIndex(productID);
        return null != products.remove(index);
    }

    public void updateProduct(int productID, Product updatedProduct) {
        // Find the product in the ArrayList
        Product toUpdate = lookupProduct(productID);

        // Get the index of the item in the ArrayList so we can update it
        int index = getProductIndex(toUpdate.getProductId());

        //Update the Product in the ArrayList
        products.set(index, updatedProduct);
    }

    public void addPart(Part part) {
        parts.add(part);
    }
    public boolean deletePart(Part part) {
        return parts.remove(part);
    }

    public Part lookupPart(int partId) {
        for (Part part: parts) {
            if (part.getPartId() == partId) {
                return part;
            }
        }
        return null;
    }

    private int getPartIndex(int partId) {
        for (Part searchPart:parts) {
            if (searchPart.getPartId() == partId) {
                return parts.indexOf(searchPart);
            }

        }
        return -1;
    }

    public void updatePart(int partId, Part updatedPart) {
        // Find the inventory.model.Part in the ArrayList
        inventory.model.Part toUpdate = lookupPart(partId);

        // Find the index of the inventory.model.Part
        int index = getPartIndex(partId);

        //Update the inventory.model.Part
        parts.set(index, updatedPart);
    }

}
