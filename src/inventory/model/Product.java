package inventory.model;

/**
 * Created by dustin on 7/23/2017.
 */


import javafx.beans.property.*;

import java.util.ArrayList;


public class Product {

    private ArrayList<Part> parts;
    private IntegerProperty ProductId  = new SimpleIntegerProperty(0);
    private StringProperty Name  = new SimpleStringProperty("");
    private DoubleProperty Price  = new SimpleDoubleProperty(0.0);
    private IntegerProperty InStock = new SimpleIntegerProperty(0);
    private IntegerProperty Min = new SimpleIntegerProperty(0);
    private IntegerProperty Max = new SimpleIntegerProperty(0);

    public Product(int productID, String name, double price, int inStock, int min, int max, ArrayList parts) {
        parts = new ArrayList<Part>();
        setProductId(productID);
        setName(name);
        setPrice(price);
        setInStock(inStock);
        setMin(min);
        setMax(max);
    }


    public void setProductId(int productId){
        ProductId.set(productId);
    }

    public int getProductId(){
        return ProductId.get();
    }


    public String getName() {
        return Name.get();
    }

    public void setName(String name) {
        Name.set(name);
    }

    public double getPrice() {
        return Price.get();
    }

    public DoubleProperty priceProperty() {
        return Price;
    }

    public void setPrice(double price) {
        Price.set(price);
    }

    public int getInStock() {
        return InStock.get();
    }

    public void setInStock(int inStock) {
        InStock.set(inStock);
    }

    public int getMin() {
        return Min.get();
    }

    public void setMin(int min) {
        Min.set(min);
    }

    public int getMax() {
        return Max.get();
    }

    public void setMax(int max) {
        Max.set(max);
    }

    public void addAssociatedPart(Part part){
        parts.add(part);
    }

    public boolean removeAssociatedPart(int partID) {
        Part toRemove = lookupAssociatedPart(partID);
        return parts.remove(toRemove);
    }

    public Part lookupAssociatedPart(int partID){

        for (Part part: parts) {
            if (part.getPartId() == partID) {
                return part;
            }
        }
        return null;
    }

    //Added so we can call this when creating new products
    public boolean partExists(int partID) {
        for (Part part: parts) {
            if (part.getPartId() == partID) {
                return true;
            }
        }
        return false;
    }

    public void setParts(ArrayList<Part> parts) {
        this.parts = parts;
    }

    public ArrayList<Part> getAssociatedParts() {
        return parts;
    }


}
