package inventory.model; /**
 * Created by dustin on 7/23/2017.
 */

import javafx.beans.property.*;

public class Part {

    private IntegerProperty PartId  = new SimpleIntegerProperty(0);
    private StringProperty Name  = new SimpleStringProperty("");
    private DoubleProperty Price  = new SimpleDoubleProperty(0.0);
    private IntegerProperty InStock = new SimpleIntegerProperty(0);
    private IntegerProperty Min = new SimpleIntegerProperty(0);
    private IntegerProperty Max = new SimpleIntegerProperty(0);

    public Part(){
        this(0, "", 0.0, 0, 0, 0);
    }

    public Part(int partId, String name, double price, int inStock, int min, int max) {
        setPartId(partId);
        setName(name);
        setPrice(price);
        setInStock(inStock);
        setMax(max);
        setMin(min);
    }

    public int getPartId() {
        return PartId.get();
    }

    public void setPartId(int partId) {
        PartId.set(partId);
    }

    public String getName() {
        return Name.get();
    }

    public void setName(String name) {
        Name.set(name);
    }

    public double getPrice() {
        return Price.get();
    }

    public void setPrice(double price) {
        Price.set(price);
    }

    public int getInStock() {
        return InStock.get();
    }

    public void setInStock(int inStock) {
        InStock.set(inStock);
    }

    public int getMin() {
        return Min.get();
    }

    public void setMin(int min) {
        Min.set(min);
    }

    public int getMax() {
        return Max.get();
    }

    public void setMax(int max) {
        Max.set(max);
    }

}
