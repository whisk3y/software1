package inventory.model;

import javafx.beans.property.*;

/**
 * Created by dustin on 7/23/2017.
 */
public class Inhouse extends Part{



    private SimpleIntegerProperty MachineID = new SimpleIntegerProperty(0);

    public Inhouse(int partId, String name, double price, int inStock, int min, int max, int machineID) {
        super(partId, name, price, inStock, min, max);
        setMachineID(machineID);
    }

    public int getMachineID() {
        return MachineID.get();
    }

    public void setMachineID(int machineId) {
        MachineID.set(machineId);
    }


}
