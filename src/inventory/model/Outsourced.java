package inventory.model;

import javafx.beans.property.*;

/**
 * Created by dustin on 7/23/2017.
 */
public class Outsourced extends Part{

    private StringProperty CompanyName = new SimpleStringProperty("");

    public Outsourced(int partId, String name, double price, int inStock, int min, int max, String companyName) {
        super(partId, name, price, inStock, min, max);
        setCompanyName(companyName);
    }


    public String getCompanyName() {
        return CompanyName.get();
    }

    public void setCompanyName(String companyName) {
        CompanyName.set(companyName);
    }

}
